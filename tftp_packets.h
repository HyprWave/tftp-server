struct RQ_packet {
    short Opcode; /* 1 for RRQ and 2 for WRQ */
    char *Filename;
    char *Mode;
};

struct DATA_packet {
    short Opcode; /* = 3 */
    unsigned short Block_num;
    char Data[512];
};

struct ACK_packet {
    short Opcode; /* = 4 */
    unsigned short Block_num;
};

struct ERROR_packet {
    short Opcode; /* = 5 */
    short ErrorCode;
    char *ErrMsg;
};
