#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include "tftp_packets.h"
#define PORT "6900"
#define TIMEOUT 5*60;
#define RETRIES 5
#define DEBUG

#ifdef DEBUG
#define printd printf
#else
#define printd(format, args...) ((void)0)
#endif
#define ERROR(args...) fprintf(stderr, args)

static struct addrinfo *myaddr;
static int client_sock = -1;
struct sockaddr_in client_addr;
socklen_t addr_len;
static char cur_packet[516]; /* current packet we send */
static char client_p[516]; /* last packet we recieved from client. */
static unsigned short block_num = 0;
static char filename[256];

short random_bind();
int handle_read();
int handle_write();
int send_error(short, char*);

inline int init_client_socket(int client_sock) {
    struct timeval timeout;
    timeout.tv_sec = TIMEOUT;
    timeout.tv_usec = 0;

    if (setsockopt(client_sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof timeout) < 0 ) {
        return -1;
    }
    if (setsockopt(client_sock, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof timeout) < 0 ) {
        return -1;
    }
    return 0;
}

inline void assign_short(char *arr, short s) {
    *arr = s & 0xff;
    *(arr + 1) = (s >> 8) & 0xff;
}

int handle_client() {
    unsigned short port;
    struct RQ_packet *rq;
    short op;
    char *mode;
    if ((port = random_bind()) == -1) {
        ERROR("Failed to bind client socket.\n");
        goto err_client;
    }
    if (connect(client_sock, (struct sockaddr *)&client_addr, addr_len) == -1) {
        printd("Failed to connect to client, dropping\n");
        goto err_client;
    }
    printd("Binded to local port %d\n", port);
    if (init_client_socket(client_sock) == -1) {
        ERROR("Failed to set timeout on client socket.\n");
        goto err_client;
    }
    rq = (struct RQ_packet *)client_p;
    op = ntohs(rq->Opcode);
    strncpy(filename, client_p + sizeof(short), sizeof filename);
    mode = client_p + sizeof(short) + strlen(filename) + 1;
    if (strncmp(mode, "octet", 5) != 0) {
        ERROR("Client uses unsupported mode, %s. Expected octet mode. Dropping connection...\n", mode);
        send_error(0, "Server only handles octet mode");
        goto err_client;
    }
    if (op == 1) {
        /* Read Request */
        printd("Client wants to read %s!\n", filename);
        handle_read();
    }
    else if (op == 2) {
        /* Write Request */
        printd("Client wants to send %s!\n", filename);
        handle_write();
    }
    else {
        ERROR("Client sent unexpected request.\n");
        send_error(4, NULL);
        goto err_client;
    }
    close(client_sock);
    return 0;
err_client:
    close(client_sock);
    return -1;
}

int c_send(size_t size) {
    int tries = RETRIES;
    int n;
    while (tries > 0) {
        /*n = sendto(client_sock, cur_packet, size, 0,
          (struct sockaddr *)&client_addr, addr_len);*/
        n = send(client_sock, cur_packet, size, 0);
        if (n > 0)
            return n;
        if (errno != EAGAIN || errno != EWOULDBLOCK) {
            return -1;
        }
        tries--;
    }
    return -1;
}
int c_recv(size_t size) {
    int tries = RETRIES;
    int n;
    memset(client_p, 0, size);
    while (tries > 0) {
        /*n = recvfrom(client_sock, client_p, size, 0,
          (struct sockaddr *)&client_addr, &addr_len);*/
        n = recv(client_sock, client_p, size, 0);
        if (n > 0)
            return n;
        if (errno != EAGAIN || errno != EWOULDBLOCK) {
            return -1;
        }
        tries--;
    }
    return -1;
}

int handle_read() {
    FILE *file;
    int char_read;
    char buf[512]; /* buffer for file. */
    int n;
    struct ACK_packet *ack;
    unsigned short op_p, block_num_p = 0;
    memset(cur_packet, 0, sizeof cur_packet);
    if ((file = fopen(filename, "rb")) == NULL) {
        send_error(1, NULL);
        return -1;
    }
    block_num = 1;
    while ((char_read = fread(buf, sizeof(char), 512, file))) {
        if (ferror(file) != 0) {
            send_error(0, "Error while reading file.");
            goto err_read;
        }
        printd("Read %d bytes\n", char_read);
        assign_short(cur_packet, htons(3));
        assign_short(cur_packet + 2, htons(block_num));
        memcpy(cur_packet + 4, buf, char_read);
        n = c_send(4 + char_read);
        if (n == -1) {
            send_error(0, "Fail to send");
            goto err_read;
        }
        printd("Sent DATA packet, block_num %d\n", block_num);
        /* Now we need to get an ACK */
        while (1) {
            memset(client_p, 0, 4);
            n = c_recv(4);
            if (n == -1) {
                printd("Failed to get ack\n");
                send_error(0, "Failed to get ACK");
                goto err_read;
            }
            ack = (struct ACK_packet *)client_p;
            op_p = ntohs(ack->Opcode);
            block_num_p = ntohs(ack->Block_num);
            if (op_p != 4) {
                /* Not an ACK, it;s illiegal */
                printd("Not an ack, recieved op %d\n", op_p);
                send_error(4, NULL);
                goto err_read;
            }
            else if (block_num_p == block_num) {
                /* We got an ACK for the DATA */
                printd("got an ACK\n");
                break;
            }
            else if (block_num_p == block_num - 1) {
                /* The DATA packet we just sent was lost, so we resend it */
                n = c_send(4 + char_read);
                send_error(0, "Timeout");
                continue;
            }
            else {
                /* We got an ack for a wrong block num, and we don't have the packet so we're out of sync */
                send_error(0, "Out of sync");
            }
        }
        block_num++; 
        if (feof(file) != 0) {
            break;
        }
    }
err_read:
    fclose(file);
    return -1;
}

int handle_write() {
    FILE *file;
    int n;
    struct DATA_packet *dat;
    unsigned short op_p, block_num_p = 0;
    short data_len = 0;
    memset(cur_packet, 0, sizeof cur_packet);
    if ((file = fopen(filename, "wx")) == NULL) {
        if (errno == EEXIST) {
            send_error(6, NULL);
        } else
            send_error(2,NULL);
        return -1;
    }
    /* Send first ACK */
    assign_short(cur_packet, htons(4));
    assign_short(cur_packet + 2, htons(block_num));
    block_num = 1;
    n = c_send(4);
    if (n < 0) {
        send_error(0, "Failed to send ack");
        printd("failed to send ack\n");
        return -1;
    }
    printd("sent first ack\n");
    do {
        while (1) {
            n = c_recv(516);
            if ( n < 0) {
                send_error(0, "Timeout");
                goto err_write;
            }
            printd("Recieved %d bytes\n", n);
            dat = (struct DATA_packet *)client_p;
            op_p = ntohs(dat->Opcode);
            block_num_p = ntohs(dat->Block_num);
            printd("op: %d b#: %d, exp. %d \n", op_p, block_num_p, block_num);
            if (op_p != 3 || block_num_p == 0) {
                /* Not a DATA, it;s illiegal */
                printd("Not a data, recieved op %d\n", op_p);
                send_error(4, NULL);
                goto err_write;
            }
            else if (block_num_p == block_num) {
                /* We got DATA */
                printd("got DATA\n");
                break;
            }
            else if (block_num_p == block_num - 1) {
                /* The ACK packet we just sent was lost, so we resend.*/
                n = c_send(4);
                if (n == -1) {
                    send_error(0, "Timeout");
                    goto err_write;
                }
                continue;
            }
        }
        data_len = n - 4;
        if (fwrite(client_p + 4, 1, data_len, file) == -1) {
            send_error(3, NULL);
            goto err_write;
        }
        /* Send ACK */
        assign_short(cur_packet, htons(4));
        assign_short(cur_packet + 2, htons(block_num));
        n = c_send(4);
        if (n < 0) {
            send_error(0, "Failed to send ack");
            printd("failed to send ack\n");
            goto err_write;
        }
        printd("sent ACK\n");
        block_num++;
    } while (data_len == 512);
    fclose(file);
    return 0;
err_write:
    fclose(file);
    return -1;
}

static char * errmsgs[] = {
    "Undefined error code",
    "File not found",
    "Access denied",
    "Disk full or allocation exceeded",
    "Illegal TFTP operation",
    "Unknown transfer ID",
    "File already exists",
    "No such user",
};

int send_error(short err, char *msg) {
    int n;
    char *errmsg;
    if (err == 0)
        errmsg = msg;
    else
        errmsg = errmsgs[err];
    memset(cur_packet, 0, sizeof cur_packet);
    assign_short(cur_packet, htons(5));
    assign_short(cur_packet + 2, htons(err));

    strncpy(cur_packet + 4, errmsg, strlen(errmsg));
    n = sendto(client_sock, cur_packet, 4 + strlen(errmsg) + 1, 0,
            (struct sockaddr *)&client_addr, addr_len);
    return n > 0;
}

int main ( int argc, char *argv[] )
{
    int server_sock, rv;
    int n = 0;
    struct addrinfo hints, *servinfo, *p;
    char s[INET_ADDRSTRLEN];
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags = AI_PASSIVE;

    if ((rv = getaddrinfo(NULL, PORT, &hints, &servinfo)) != 0) {
        fprintf(stderr, "Error calling getaddrinfo\n");
        return 1;
    }

    for (p = servinfo; p != NULL ; p = p->ai_next) {
        if ((server_sock = socket(p->ai_family, p->ai_socktype, p->ai_protocol))
                == -1) {
            continue;
        }
        if (bind(server_sock, p->ai_addr, p->ai_addrlen) == 0) {
            break;
        }
        close(server_sock);
    }

    if (p == NULL ) {
        freeaddrinfo(servinfo);
        fprintf(stderr, "Failed to bind\n");
        return 1;
    }
    myaddr = p;
    /* Let's get a client! */
    while (1) {
        printd("Listening for a client...\n");
        addr_len = sizeof client_addr;
        n = recvfrom(server_sock, client_p, 516, 0,
                (struct sockaddr *)&client_addr, &addr_len);
        if (n < 0) {
            if (errno == EINTR)
                continue;
        }
        printd("Got a client!\n");
        printd("Client addr: %s:%d\n", inet_ntop(client_addr.sin_family, &client_addr.sin_addr, s, sizeof s), ntohs(client_addr.sin_port));
        client_sock = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
        if (client_sock == -1) {
            ERROR("Couldn't open a socket to handle client.\n");
        }
        handle_client();
        block_num = 0;
    }
    close(server_sock);
    return EXIT_SUCCESS;
}

short random_bind() {
    unsigned short first_port, port;
    int tries = 5;
    first_port = port = rand() % 65535;
    do {
        ((struct sockaddr_in *)(myaddr->ai_addr))->sin_port = htons(port); /* setting the port */
        if (bind(client_sock, myaddr->ai_addr, myaddr->ai_addrlen) < 0) {
            if (errno != EINVAL || errno != EADDRINUSE) { /* if there is an error that is not about port already in use, quit. */
                return -1;
            }
        } else {
            return port;
        }
        tries--;
        port++;
        if (port > 65535)
            port = 65535;
    } while (port != first_port); /* if port == first_port it means we tried all ports, so something is wrong. */

    return -1;
}
